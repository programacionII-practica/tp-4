package situacion;
/*
Tomando como base la situación 2 del práctico 3, se pide realizar lo indicado el apartado ejercicios,
agregando la siguiente funcionalidad.
Ahora la Agencia de alquiler de vehículos amplía sus servicios, pudiendo vender autos 0 Km y usados.
El precio de los autos 0 km se compone de un precio base 
más el valor correspondiente a los componentes extras que se dese agregar,
(aire acondicionado 2%, levanta cristales eléctricos 5%, alarma un 1%), más el 50% de utilidad. 
El precio de los usados se obtiene a partir del precio base más un 35% de utilidad. 
Los autos 0 km no se pueden alquilar, en cambio los autos usados sí pueden ser vendidos.
Se desea poder ver la cotización de los autos a vender ordenados por precio, como así también el costo
de un alquiler de un vehículo. 
Esta nueva funcionalidad se debe ver reflejado la solución implementada para la situación mencionada
*/


public class Agencia 
{
    private String nombreAgencia;
    private ParqueAutomotor parqueAutomotor;

    public Agencia(String nombreAgencia) {
        this.nombreAgencia = nombreAgencia;
        parqueAutomotor = new ParqueAutomotor();
    }
    public String getNombreAgencia()
    {
        return this.nombreAgencia;
    }

    public ParqueAutomotor getParqueAutomotor() {
        return parqueAutomotor;
    }
}
