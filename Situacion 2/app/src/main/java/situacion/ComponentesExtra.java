/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package situacion;

import excepciones.ComponenteInvalidoException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gustavo Contreras
 */
public class ComponentesExtra 
{
    private List<String> componentes = new ArrayList<>();

    public void addComponente(String componente) throws ComponenteInvalidoException
    {
        if (componente.equals("Aire acondicionado") || componente.equals("Alarma") || componente.equals("Levanta cristales"))
        {
            componentes.add(componente);
        }
        else
        {
            throw new ComponenteInvalidoException("Se intento agregar un componente invalido al auto");
        }
    }
    public List<String> getComponentes() 
    {
        return componentes;
    }  
    
}
