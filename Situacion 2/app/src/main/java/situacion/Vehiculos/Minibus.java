/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package situacion.Vehiculos;

import Interfaces.PrecioAlquiler;
import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;

/**
 *
 * @author Gustavo Contreras
 */
public class Minibus extends VehiculoPasajero implements PrecioAlquiler
{
    public final int PRECIO_SEGURO;

    public Minibus(int numeroPlazas, String marca, String patente, int precioAlquilerPorDia) throws PrecioIncorrectoException, NumeroPlazasException{
        super(numeroPlazas, marca, patente, precioAlquilerPorDia);
        this.PRECIO_SEGURO = 250;
    }

    @Override
    public float precioAlquiler(int diasAlquilado) {
        return (float)((this.getPrecioAlquilerPorDia() * diasAlquilado) + (this.getPRECIO_POR_PLAZA_Y_DIA() * this.getNumeroPlazas()) + (this.getPRECIO_SEGURO() * this.getNumeroPlazas()));
    }

    public int getPRECIO_SEGURO() {
        return PRECIO_SEGURO;
    }
}
