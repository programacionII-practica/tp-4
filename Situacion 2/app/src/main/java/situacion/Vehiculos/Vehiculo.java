package situacion.Vehiculos;

import excepciones.PrecioIncorrectoException;

public abstract class Vehiculo 
{
    private String marca;
    private String patente;
    private int precioAlquilerPorDia;

    public Vehiculo(String marca, String patente, int precioAlquilerPorDia) throws PrecioIncorrectoException{
        if (precioAlquilerPorDia <= 0)
        {
            throw new PrecioIncorrectoException("Se ingreso un precio de alquiler incorrecto para vehiculo.");
        }
        else
        {
            this.marca = marca;
            this.patente = patente;
            this.precioAlquilerPorDia = precioAlquilerPorDia;
        }
        
    }

    public String getMarca() {
        return marca;
    }

    public String getPatente() {
        return patente;
    }

    public int getPrecioAlquilerPorDia() {
        return precioAlquilerPorDia;
    }


}
