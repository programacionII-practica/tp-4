/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package situacion.Vehiculos;

import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;

/**
 *
 * @author Gustavo Contreras
 */
public abstract class VehiculoDeCarga extends Vehiculo
{
    private final float PRECIO_POR_KILOMETRAJE;

    public VehiculoDeCarga(int kilometraje, String marca, String patente, int precioAlquilerPorDia) throws PrecioIncorrectoException, NumeroPlazasException{
        super(marca, patente, precioAlquilerPorDia);
        if (kilometraje >= 50)
        {
            PRECIO_POR_KILOMETRAJE = 20*kilometraje;
        }
        else
        {
            PRECIO_POR_KILOMETRAJE = 300;
        }
    }

    public float getPRECIO_POR_KILOMETRAJE() {
        return PRECIO_POR_KILOMETRAJE;
    }
}
