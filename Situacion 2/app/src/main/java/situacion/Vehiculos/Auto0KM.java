/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package situacion.Vehiculos;

import excepciones.ComponenteInvalidoException;
import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;
import situacion.ComponentesExtra;

/**
 *
 * @author Gustavo Contreras
 */
public class Auto0KM extends Auto
{
    private ComponentesExtra componentes = new ComponentesExtra();

    public Auto0KM(int precioVentaBase, int numeroPlazas, String marca, String patente, int precioAlquilerPorDia) throws PrecioIncorrectoException, NumeroPlazasException{
        super(precioVentaBase, numeroPlazas, marca, patente, precioAlquilerPorDia);
    }
    public void agregarComponente(String componente) throws ComponenteInvalidoException
    {
        componentes.addComponente(componente);
    }

    @Override
    public float precioVenta() {
        float precio;
        precio = (float) (this.getPrecioVentaBase() + (this.getPrecioVentaBase()*0.5));
        for (String componente : this.componentes.getComponentes())
        {
            switch (componente) {
                case "Aire acondicionado":
                    precio += this.getPrecioVentaBase() * 0.02;
                    break;
                case "Alarma":
                    precio += this.getPrecioVentaBase() * 0.01;
                    break;
                case "Levanta cristales":
                    precio += this.getPrecioVentaBase() * 0.05;
                    break;
            }
        }
        return precio;
    }
    public ComponentesExtra getComponentes() {
        return componentes;
    }

    
}
