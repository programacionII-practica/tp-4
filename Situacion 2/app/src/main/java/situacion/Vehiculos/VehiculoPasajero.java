/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package situacion.Vehiculos;

import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;


/**
 *
 * @author Gustavo Contreras
 */
public abstract class VehiculoPasajero extends Vehiculo 
{
    private final int PRECIO_POR_PLAZA_Y_DIA = 50;
    private int numeroPlazas;

    public VehiculoPasajero(int numeroPlazas, String marca, String patente, int precioAlquilerPorDia) throws PrecioIncorrectoException, NumeroPlazasException{
        super(marca, patente, precioAlquilerPorDia);
        if (numeroPlazas > 0)
        {
            this.numeroPlazas = numeroPlazas;
        }
        else
        {
            throw new NumeroPlazasException("Se ingreso un numero de plazas invalido");
        }
    }

    public VehiculoPasajero(String marca, String patente, int precioAlquilerPorDia) throws PrecioIncorrectoException{
        super(marca, patente, precioAlquilerPorDia);
    }

    public int getPRECIO_POR_PLAZA_Y_DIA() {
        return PRECIO_POR_PLAZA_Y_DIA;
    }

    public int getNumeroPlazas() {
        return numeroPlazas;
    }
    
}
