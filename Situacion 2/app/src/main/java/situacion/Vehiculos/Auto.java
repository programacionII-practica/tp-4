package situacion.Vehiculos;

import Interfaces.PrecioVenta;
import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;

public abstract class Auto extends VehiculoPasajero implements PrecioVenta
{
    private int precioVentaBase;

    public Auto(int precioVentaBase, int numeroPlazas, String marca, String patente, int precioAlquilerPorDia) throws PrecioIncorrectoException, NumeroPlazasException{
        super(numeroPlazas, marca, patente, precioAlquilerPorDia);
        if (precioVentaBase > 0)
        {
            this.precioVentaBase = precioVentaBase;
        }
        else
        {
            throw new PrecioIncorrectoException("Se ingreso un precio de venta negativo o nulo");
        }
    }
    
    public Auto(String marca, String patente, int precioAlquilerPorDia) throws PrecioIncorrectoException{
        super(marca, patente, precioAlquilerPorDia);
    }

    public int getPrecioVentaBase() {
        return precioVentaBase;
    }
}
