/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package situacion.Vehiculos;

import Interfaces.PrecioAlquiler;
import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;

/**
 *
 * @author Gustavo Contreras
 */
public class AutoUsado extends Auto implements PrecioAlquiler
{
    public AutoUsado(int precioVentaBase, int numeroPlazas, String marca, String patente, int precioAlquilerPorDia) throws PrecioIncorrectoException, NumeroPlazasException{
        super(precioVentaBase, numeroPlazas, marca, patente, precioAlquilerPorDia);
    }    
    public AutoUsado(String marca, String patente, int precioAlquilerPorDia) throws PrecioIncorrectoException
    {
        super(marca,patente,precioAlquilerPorDia);
    }
    @Override
    public float precioAlquiler(int diasAlquilado) {
        return (float)((this.getPrecioAlquilerPorDia() * diasAlquilado) + (this.getPRECIO_POR_PLAZA_Y_DIA() * this.getNumeroPlazas()));
    }

    @Override
    public float precioVenta() {
        return (float)(this.getPrecioVentaBase() + (this.getPrecioVentaBase()*0.35));
    }
}
