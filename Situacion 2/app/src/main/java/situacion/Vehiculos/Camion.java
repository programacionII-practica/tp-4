/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package situacion.Vehiculos;

import Interfaces.PrecioAlquiler;
import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;

/**
 *
 * @author Gustavo Contreras
 */
public class Camion extends VehiculoDeCarga implements PrecioAlquiler
{
    public final int PRECIO_ABONO = 200;

    public Camion(int kilometraje, String marca, String patente, int precioAlquilerPorDia) throws PrecioIncorrectoException, NumeroPlazasException{
        super(kilometraje, marca, patente, precioAlquilerPorDia);
    }

    public int getPRECIO_ABONO() {
        return PRECIO_ABONO;
    }

    @Override
    public float precioAlquiler(int diasAlquilado) {
        return (float)((this.getPrecioAlquilerPorDia() * diasAlquilado) + (this.getPRECIO_POR_KILOMETRAJE() + this.getPRECIO_ABONO()));
    }
    
}
