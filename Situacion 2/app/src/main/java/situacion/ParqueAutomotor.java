/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package situacion;

import situacion.Vehiculos.Vehiculo;
import situacion.Vehiculos.Auto;
import situacion.Vehiculos.AutoUsado;
import situacion.Vehiculos.Auto0KM;
import Interfaces.PrecioAlquiler;
import excepciones.ListaAutoVaciaException;
import excepciones.ListaVehiculoVaciaException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Gustavo Contreras
 */
public class ParqueAutomotor 
{
    private List<Vehiculo> vehiculosEnAlquiler = new ArrayList<>();
    private List<Auto> autos = new ArrayList<>();
    
    public void altaVehiculo(Vehiculo vehiculo)
    {
        if (PrecioAlquiler.class.isAssignableFrom(vehiculo.getClass()))
        {
            Vehiculo vehiculoEnAlquiler = (Vehiculo) vehiculo;
            vehiculosEnAlquiler.add(vehiculoEnAlquiler);
        }
        if (vehiculo instanceof Auto0KM || vehiculo instanceof AutoUsado)
        {
            Auto auto = (Auto) vehiculo;
            altaAuto(auto);
        }
    }
    private void altaAuto(Auto auto)
    {
        autos.add(auto);
    }
    
    public boolean existeVehiculo(String patente)
    {
        boolean existeVehiculo = false;
        for (Vehiculo vehiculo : this.vehiculosEnAlquiler)
        {
            if (vehiculo.getPatente().equals(patente))
            {
                existeVehiculo = true;
            }
        }
        return existeVehiculo;
    }
    public Vehiculo buscarVehiculo(String patente)
    {
        Vehiculo vehiculoEncontrado = null;
        if (existeVehiculo(patente))
        {
            for (Vehiculo vehiculo : this.vehiculosEnAlquiler)
            {
                if (vehiculo.getPatente().equals(patente))
                {
                    vehiculoEncontrado = vehiculo;
                }
            }
            
        }
        return vehiculoEncontrado;
    }
    public Auto buscarAuto(String patente)
    {
       Auto autoEncontrado = null;
       if (existeAuto(patente))
        {
            for (Auto auto : this.autos)
            {
                if (auto.getPatente().equals(patente))
                {
                    autoEncontrado = auto;
                }
            }
            
        }
       return autoEncontrado;
    }
    public boolean existeAuto(String patente)
    {
        boolean existeAuto = false;
        for (Auto auto : this.autos)
        {
            if (auto.getPatente().equals(patente))
            {
                existeAuto = true;
            }
        }
        return existeAuto;
    }
    public List<Vehiculo> listarVehiculosPorPrecio() throws ListaVehiculoVaciaException
    {
        if (!this.vehiculosEnAlquiler.isEmpty())
        {
            Collections.sort(this.vehiculosEnAlquiler, new Comparator<Vehiculo>()
            {
                @Override
                public int compare(Vehiculo o1, Vehiculo o2) {
                    return Integer.valueOf(o1.getPrecioAlquilerPorDia()).compareTo(o2.getPrecioAlquilerPorDia());
                }
                
            });
            return this.vehiculosEnAlquiler;
        }
        else
        {
            throw new ListaVehiculoVaciaException("No hay vehiculos disponibles para alquilar");
        }
    }
    public List<Auto> listarAutosPorPrecio() throws ListaAutoVaciaException
    {
        if (!this.autos.isEmpty())
        {
            Collections.sort(this.autos, new Comparator<Auto>()
            {
                @Override
                public int compare(Auto o1, Auto o2) 
                {
                    float precioAuto1;
                    if (o1 instanceof Auto0KM)
                    {
                        Auto0KM auto0KM = (Auto0KM)o1;
                        precioAuto1 = auto0KM.precioVenta();
                    }
                    else
                    {
                        AutoUsado autoUsado = (AutoUsado)o1;
                        precioAuto1 = autoUsado.precioVenta();
                    }
                    float precioAuto2;
                    if (o2 instanceof Auto0KM)
                    {
                        Auto0KM auto0KM = (Auto0KM)o2;
                        precioAuto2 = auto0KM.precioVenta();
                    }
                    else
                    {
                        AutoUsado autoUsado = (AutoUsado)o2;
                        precioAuto2 = autoUsado.precioVenta();
                    }
                    return Float.valueOf(precioAuto1).compareTo(precioAuto2);
                }
                
            });
            return this.autos;
        }
        else
        {
            throw new ListaAutoVaciaException("No hay autos disponibles para vender");
        }
    }
    
    public void bajaVehiculo(Vehiculo vehiculo)
    {
        if (existeVehiculo(vehiculo.getPatente()))
        {
            this.vehiculosEnAlquiler.remove(buscarVehiculo(vehiculo.getPatente()));
        }
        
    }
    
    public void bajaAuto(Auto auto)
    {
        if (existeAuto(auto.getPatente()))
        {
            Auto autoEliminado=null;
            for (Auto autoAEliminar : this.autos)
            {
                if (autoAEliminar.getPatente().equals(auto.getPatente()))
                {
                    autoEliminado = autoAEliminar;
                }
            }
            this.autos.remove(autoEliminado);
        }
    }
}
