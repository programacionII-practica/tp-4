package excepciones;

public class PrecioIncorrectoException extends Exception
{
    public PrecioIncorrectoException(String message) {
        super(message);
    }
}
