package excepciones;

public class ComponenteInvalidoException extends Exception{
    public ComponenteInvalidoException(String message) {
        super(message);
    }
}
