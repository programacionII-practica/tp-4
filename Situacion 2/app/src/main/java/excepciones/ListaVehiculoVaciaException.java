package excepciones;

public class ListaVehiculoVaciaException extends Exception{
    public ListaVehiculoVaciaException(String message) {
        super(message);
    }
}
