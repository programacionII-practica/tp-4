package excepciones;

public class ListaAutoVaciaException extends Exception{
    public ListaAutoVaciaException(String message) {
        super(message);
    }
}
