package excepciones;

public class NumeroPlazasException extends Exception{

    public NumeroPlazasException(String message) {
        super(message);
    }
    
}
