/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package excepciones;

import org.junit.Test;

import situacion.Vehiculos.Auto0KM;

import static org.junit.Assert.*;

/**
 *
 * @author Gustavo Contreras
 */
public class ComponenteInvalidoExceptionTest {
    
    @Test (expected = ComponenteInvalidoException.class)
    public void componenteInvalidoTest() throws PrecioIncorrectoException, NumeroPlazasException, ComponenteInvalidoException 
    {
        Auto0KM auto = new Auto0KM(200, 4, "Ford", "OWO-404", 100);
        auto.agregarComponente("Escalera");
    }
    
}
