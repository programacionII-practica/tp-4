package situacion;

import excepciones.ListaAutoVaciaException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import excepciones.ListaVehiculoVaciaException;
import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;
import situacion.Vehiculos.Auto0KM;
import situacion.Vehiculos.AutoUsado;
import situacion.Vehiculos.Camion;
import situacion.Vehiculos.Minibus;

public class ParqueAutomotorTest {
    @Test
    public void testAltaVehiculo() throws PrecioIncorrectoException, NumeroPlazasException, ListaVehiculoVaciaException 
    {
        AutoUsado auto = new AutoUsado(200, 4, "Ford", "OWO-404", 50);
        Minibus minibus = new Minibus(20, "Volkswagen", "UGO-183", 500);

        Agencia agencia = new Agencia("Estebanquito");
        agencia.getParqueAutomotor().altaVehiculo(auto);
        agencia.getParqueAutomotor().altaVehiculo(minibus);

        assertTrue(agencia.getParqueAutomotor().listarVehiculosPorPrecio().get(0).getMarca().equals("Ford"));
        assertTrue(agencia.getParqueAutomotor().listarVehiculosPorPrecio().get(1).getMarca().equals("Volkswagen"));
    }

    @Test
    public void testBajaAuto() throws PrecioIncorrectoException, NumeroPlazasException, ListaVehiculoVaciaException, ListaAutoVaciaException 
    {
        AutoUsado auto = new AutoUsado(200, 4, "Ford", "OWO-404", 50);
        Auto0KM auto2 = new Auto0KM(4000, 4, "Volkswagen", "AGA-707", 1);

        Agencia agencia = new Agencia("Estebanquito");
        agencia.getParqueAutomotor().altaVehiculo(auto);
        agencia.getParqueAutomotor().altaVehiculo(auto2);

        agencia.getParqueAutomotor().bajaAuto(auto);
        assertTrue(agencia.getParqueAutomotor().listarAutosPorPrecio().get(0).getMarca().equals("Volkswagen"));
    }

    @Test
    public void testBajaVehiculo() throws PrecioIncorrectoException, NumeroPlazasException, ListaVehiculoVaciaException 
    {
        AutoUsado auto = new AutoUsado(200, 4, "Ford", "OWO-404", 50);
        Minibus minibus = new Minibus(20, "Volkswagen", "UGO-183", 500);

        Agencia agencia = new Agencia("Estebanquito");
        agencia.getParqueAutomotor().altaVehiculo(auto);
        agencia.getParqueAutomotor().altaVehiculo(minibus);

        agencia.getParqueAutomotor().bajaVehiculo(minibus);
        assertTrue(agencia.getParqueAutomotor().listarVehiculosPorPrecio().get(0).getMarca().equals("Ford"));
    }

    @Test
    public void testBuscarAuto() throws PrecioIncorrectoException, NumeroPlazasException 
    {
        AutoUsado auto = new AutoUsado(200, 4, "Ford", "OWO-404", 50);
        Auto0KM auto2 = new Auto0KM(4000, 4, "Volkswagen", "AGA-707", 1);
        AutoUsado auto3 = new AutoUsado(3000, 2, "Toyota", "IGO-202", 20);
        Agencia agencia = new Agencia("Estebanquito");

        agencia.getParqueAutomotor().altaVehiculo(auto);
        agencia.getParqueAutomotor().altaVehiculo(auto2);
        agencia.getParqueAutomotor().altaVehiculo(auto3);

        assertEquals(auto3,agencia.getParqueAutomotor().buscarAuto(auto3.getPatente()));
    }

    @Test
    public void testBuscarVehiculo() throws PrecioIncorrectoException, NumeroPlazasException 
    {
        AutoUsado auto = new AutoUsado(200, 4, "Ford", "OWO-404", 50);
        Minibus minibus = new Minibus(20, "Volkswagen", "UGO-183", 500);
        Auto0KM auto2 = new Auto0KM(4000, 4, "Volkswagen", "AGA-707", 1);

        Agencia agencia = new Agencia("Estebanquito");

        agencia.getParqueAutomotor().altaVehiculo(auto2);
        agencia.getParqueAutomotor().altaVehiculo(minibus);
        agencia.getParqueAutomotor().altaVehiculo(auto);

        assertEquals("UGO-183", agencia.getParqueAutomotor().buscarVehiculo(minibus.getPatente()).getPatente());
    }

    @Test
    public void testExisteAuto() throws PrecioIncorrectoException, NumeroPlazasException 
    {
        AutoUsado auto = new AutoUsado(200, 4, "Ford", "OWO-404", 50);
        Auto0KM auto2 = new Auto0KM(4000, 4, "Volkswagen", "AGA-707", 1);
        Agencia agencia = new Agencia("Estebanquito");

        agencia.getParqueAutomotor().altaVehiculo(auto);

        assertTrue(agencia.getParqueAutomotor().existeAuto(auto.getPatente()));
        assertFalse(agencia.getParqueAutomotor().existeAuto(auto2.getPatente()));
    }

    @Test
    public void testExisteVehiculo() throws PrecioIncorrectoException, NumeroPlazasException 
    {
        AutoUsado auto = new AutoUsado(200, 4, "Ford", "OWO-404", 50);
        Minibus minibus = new Minibus(20, "Volkswagen", "UGO-183", 500);
        Auto0KM auto2 = new Auto0KM(4000, 4, "Volkswagen", "AGA-707", 1);

        Agencia agencia = new Agencia("Estebanquito");

        agencia.getParqueAutomotor().altaVehiculo(minibus);
        
        assertFalse(agencia.getParqueAutomotor().existeVehiculo(auto2.getPatente()));
        assertTrue(agencia.getParqueAutomotor().existeVehiculo(minibus.getPatente()));
        assertFalse(agencia.getParqueAutomotor().existeVehiculo(auto2.getPatente()));
    }

    @Test
    public void testListarAutosPorPrecio() throws PrecioIncorrectoException, NumeroPlazasException, ListaAutoVaciaException 
    {
        AutoUsado auto = new AutoUsado(200, 4, "Ford", "OWO-404", 50); //270
        Auto0KM auto2 = new Auto0KM(4000, 4, "Volkswagen", "AGA-707", 1); //6000
        AutoUsado auto3 = new AutoUsado(3000, 2, "Toyota", "IGO-202", 20); //4050
        Agencia agencia = new Agencia("Estebanquito");

        agencia.getParqueAutomotor().altaVehiculo(auto);
        agencia.getParqueAutomotor().altaVehiculo(auto2);
        agencia.getParqueAutomotor().altaVehiculo(auto3);

        assertEquals(auto.getPatente(), agencia.getParqueAutomotor().listarAutosPorPrecio().get(0).getPatente());
        assertEquals(auto3.getPatente(), agencia.getParqueAutomotor().listarAutosPorPrecio().get(1).getPatente());
        assertEquals(auto2.getPatente(), agencia.getParqueAutomotor().listarAutosPorPrecio().get(2).getPatente());
    }

    @Test
    public void testListarVehiculosPorPrecio() throws PrecioIncorrectoException, NumeroPlazasException, ListaVehiculoVaciaException {
        AutoUsado auto = new AutoUsado(200, 4, "Ford", "OWO-404", 50);
        Auto0KM auto2 = new Auto0KM(4000, 4, "Volkswagen", "AGA-707", 1); //No se alquila, es Auto 0KM
        Minibus minibus = new Minibus(20, "Volkswagen", "UGO-183", 500);// 
        Camion camion = new Camion(20 , "Volvo", "TUT-999", 400);

        Agencia agencia = new Agencia("Estebanquito");
        agencia.getParqueAutomotor().altaVehiculo(auto);
        agencia.getParqueAutomotor().altaVehiculo(auto2);
        agencia.getParqueAutomotor().altaVehiculo(minibus);
        agencia.getParqueAutomotor().altaVehiculo(camion);
        
        assertEquals(minibus.getPatente(), agencia.getParqueAutomotor().listarVehiculosPorPrecio().get(2).getPatente());
        assertEquals(camion.getPatente(), agencia.getParqueAutomotor().listarVehiculosPorPrecio().get(1).getPatente());
        assertEquals(auto.getPatente(), agencia.getParqueAutomotor().listarVehiculosPorPrecio().get(0).getPatente());
    }
}
