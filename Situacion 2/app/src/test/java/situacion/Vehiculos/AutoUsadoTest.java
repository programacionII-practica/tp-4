/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package situacion.Vehiculos;


import org.junit.Test;

import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;

import static org.junit.Assert.*;

/**
 *
 * @author Gustavo Contreras
 */
public class AutoUsadoTest {
    
    /**
     * Test of precioAlquiler method, of class AutoUsado.
     * @throws NumeroPlazasException
     * @throws PrecioIncorrectoException
     */
    @Test
    public void testPrecioAlquiler() throws PrecioIncorrectoException, NumeroPlazasException {
        AutoUsado auto = new AutoUsado(200, 4, "Ford", "OWO-404", 50);
        assertEquals((int)450,(int)auto.precioAlquiler(5));
    }

    /**
     * Test of precioVenta method, of class AutoUsado.
     * @throws NumeroPlazasException
     * @throws PrecioIncorrectoException
     */
    @Test
    public void testPrecioVenta() throws PrecioIncorrectoException, NumeroPlazasException {
        AutoUsado auto = new AutoUsado(200, 4, "Ford", "OWO-404", 50);
        assertEquals((int)270,(int)auto.precioVenta());
    }
    
}
