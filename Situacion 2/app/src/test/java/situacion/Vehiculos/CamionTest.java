package situacion.Vehiculos;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;

public class CamionTest {
    @Test
    public void testPrecioAlquiler50KM() throws PrecioIncorrectoException, NumeroPlazasException 
    {
        Camion camion = new Camion(50, "Volvo","AWA-707",300);
        assertEquals((int)2700,(int) camion.precioAlquiler(5));
    }
    @Test
    public void testPrecioAlquilerMenosDe50KM() throws PrecioIncorrectoException, NumeroPlazasException 
    {
        Camion camion = new Camion(25, "Volvo","AWA-707",300);
        assertEquals((int)2000,(int) camion.precioAlquiler(5));
    }
}
