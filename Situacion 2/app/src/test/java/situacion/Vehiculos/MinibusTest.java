package situacion.Vehiculos;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;

public class MinibusTest {
    @Test
    public void testPrecioAlquiler() throws PrecioIncorrectoException, NumeroPlazasException {
        Minibus minibus = new Minibus(20, "Volkswagen", "UGO-183", 500);
        assertEquals((int)8500, (int)minibus.precioAlquiler(5));
    }
}
