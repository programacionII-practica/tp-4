package situacion.Vehiculos;

import situacion.Vehiculos.AutoUsado;
import situacion.Vehiculos.Vehiculo;

import org.junit.Test;

import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;
import static org.junit.Assert.*;

public class VehiculoTest 
{
    @Test
    public void ConstructorTest() throws PrecioIncorrectoException, NumeroPlazasException
    {
        Vehiculo vehiculo = new AutoUsado("Ford", "OWO-404", 200);
        assertEquals("Ford", vehiculo.getMarca());
        assertEquals("OWO-404",vehiculo.getPatente());
        assertEquals((int)200,(int)vehiculo.getPrecioAlquilerPorDia());
    }
    @Test (expected = PrecioIncorrectoException.class)
    public void PrecioIncorrectoExceptionTest() throws PrecioIncorrectoException, NumeroPlazasException{
        Vehiculo vehiculo = new AutoUsado("Ford", "OWO-404", -200);
    }
    @Test (expected = NumeroPlazasException.class)
    public void NumeroPlazasExceptionTest() throws PrecioIncorrectoException, NumeroPlazasException{
        Auto auto = new AutoUsado(200, 0, "Ford", "OWO-404", 200);
    }

}
