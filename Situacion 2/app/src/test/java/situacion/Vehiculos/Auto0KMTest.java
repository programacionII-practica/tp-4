package situacion.Vehiculos;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import excepciones.ComponenteInvalidoException;
import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;

public class Auto0KMTest {
    @Test
    public void testPrecioVentaSinComponente() throws PrecioIncorrectoException, NumeroPlazasException {
        Auto0KM auto = new Auto0KM(4000, 4, "Ford", "OWO-404", 1);
        assertEquals((int)6000, (int)auto.precioVenta());
    }
    @Test
    public void testPrecioVentaAire() throws PrecioIncorrectoException, NumeroPlazasException, ComponenteInvalidoException {
        Auto0KM auto = new Auto0KM(4000, 4, "Ford", "OWO-404", 1);
        auto.agregarComponente("Aire acondicionado");
        assertEquals((int)6080, (int)auto.precioVenta());
    }
    @Test
    public void testPrecioVentaLevantaCristales() throws PrecioIncorrectoException, NumeroPlazasException, ComponenteInvalidoException {
        Auto0KM auto = new Auto0KM(4000, 4, "Ford", "OWO-404", 1);
        auto.agregarComponente("Levanta cristales");
        assertEquals((int)6200, (int)auto.precioVenta());
    }
    @Test
    public void testPrecioVentaAlarma() throws PrecioIncorrectoException, NumeroPlazasException, ComponenteInvalidoException {
        Auto0KM auto = new Auto0KM(4000, 4, "Ford", "OWO-404", 1);
        auto.agregarComponente("Alarma");
        assertEquals((int)6040, (int)auto.precioVenta());
    }
    @Test
    public void testPrecioVentaDosComponentes() throws PrecioIncorrectoException, NumeroPlazasException, ComponenteInvalidoException {
        Auto0KM auto = new Auto0KM(4000, 4, "Ford", "OWO-404", 1);
        auto.agregarComponente("Alarma");
        auto.agregarComponente("Aire acondicionado");
        assertEquals((int)6120, (int)auto.precioVenta());
    }
    @Test
    public void testPrecioVentaTresComponentes() throws PrecioIncorrectoException, NumeroPlazasException, ComponenteInvalidoException {
        Auto0KM auto = new Auto0KM(4000, 4, "Ford", "OWO-404", 1);
        auto.agregarComponente("Alarma");
        auto.agregarComponente("Levanta cristales");
        auto.agregarComponente("Aire acondicionado");
        assertEquals((int)6320, (int)auto.precioVenta());
    }
}