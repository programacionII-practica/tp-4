package situacion.Vehiculos;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;

public class CamionetaTest {
    @Test
    public void testPrecioAlquiler50KM() throws PrecioIncorrectoException, NumeroPlazasException {
        Camioneta camioneta = new Camioneta(50, "Toyota", "IDI-353", 100);
        assertEquals((int)1500,(int)camioneta.precioAlquiler(5));
    }
    @Test
    public void testPrecioAlquilerMenos50KM() throws PrecioIncorrectoException, NumeroPlazasException {
        Camioneta camioneta = new Camioneta(10, "Toyota", "IDI-353", 100);
        assertEquals((int)800,(int)camioneta.precioAlquiler(5));
    }
}
