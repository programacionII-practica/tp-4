package situacion.Vehiculos;

import org.junit.Test;

import excepciones.NumeroPlazasException;
import excepciones.PrecioIncorrectoException;

public class VehiculoPasajeroTest 
{
    @Test (expected = NumeroPlazasException.class)
    public void NumeroPlazasExceptionTest() throws PrecioIncorrectoException, NumeroPlazasException
    {
        VehiculoPasajero vehiculoPasajero = new AutoUsado(2000, 0, "Ford", "OWO-404", 200);
    }
}
