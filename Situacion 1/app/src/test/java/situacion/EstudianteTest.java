package situacion;
import org.junit.Test;
import static org.junit.Assert.*;

import excepciones.AtributoEstudianteIncorrectoException;
import excepciones.CalificacionNegativaException;
import excepciones.PromedioIncalculableException;

public class EstudianteTest 
{
    @Test
    public void ConstructorEstudiante() throws CalificacionNegativaException, PromedioIncalculableException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        assertEquals("Gustavo",estudiante.getNombre());
        assertEquals((int)19,estudiante.getEdad());
        assertEquals((int)43995185,estudiante.getDNI());
        assertEquals("Capital",estudiante.getLugarNacimiento());

        estudiante.agregarCalificacion(8);
        estudiante.agregarCalificacion(7);   // Promedio = (8+5+8)/3 --> 21/3 --> 7 
        estudiante.agregarCalificacion(8);

        assertEquals((int)7,estudiante.recibirPromedio());
    }
    @Test (expected = AtributoEstudianteIncorrectoException.class)
    public void AtributoEstudianteDniIncorrectoException() throws AtributoEstudianteIncorrectoException
    {
            Estudiante estudiante = new Estudiante("Gustavo","Contreras", 19, -1, "Capital");
    }
    @Test (expected = AtributoEstudianteIncorrectoException.class)
    public void AtributoEstudianteEdadIncorrectoException() throws AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante = new Estudiante("Gustavo","Contreras", -1, 43995185, "Capital");
    }
    @Test (expected = PromedioIncalculableException.class)
    public void PromedioIncalculableException() throws CalificacionNegativaException, PromedioIncalculableException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        assertEquals("Gustavo",estudiante.getNombre());
        assertEquals((int)19,estudiante.getEdad());
        assertEquals((int)43995185,estudiante.getDNI());
        assertEquals("Capital",estudiante.getLugarNacimiento());

        assertEquals((int)7,estudiante.recibirPromedio());
    }
    @Test (expected = CalificacionNegativaException.class)
    public void CalificacionNegativaException() throws CalificacionNegativaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        assertEquals("Gustavo",estudiante.getNombre());
        assertEquals((int)19,estudiante.getEdad());
        assertEquals((int)43995185,estudiante.getDNI());
        assertEquals("Capital",estudiante.getLugarNacimiento());

        estudiante.agregarCalificacion(-1);
    }
}
