package situacion;

import org.junit.Test;

import excepciones.AtributoEstudianteIncorrectoException;
import excepciones.CalificacionNegativaException;
import excepciones.ListaEstudiantesVaciaException;
import excepciones.PromedioIncalculableException;

import static org.junit.Assert.*;

public class CursoTest 
{
    @Test
    public void ConstructorCurso()
    {
        Curso curso = new Curso("1A", 20);
        assertEquals("1A",curso.getNombreCurso());
        assertEquals((int)20,curso.getCodigo());
    }
    @Test
    public void resetearNotas() throws PromedioIncalculableException, CalificacionNegativaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo", "Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(8);
        estudiante1.agregarCalificacion(7);   
        estudiante1.agregarCalificacion(8);

        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(4);
        estudiante2.agregarCalificacion(5);   
        estudiante2.agregarCalificacion(6);

        Estudiante estudiante3 = new Estudiante("Josefina","Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(9);
        estudiante3.agregarCalificacion(10);
        estudiante3.agregarCalificacion(8);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        curso.resetearNotas();

        assertEquals((int)0,estudiante1.recibirPromedio());
        assertEquals((int)0,estudiante2.recibirPromedio());
        assertEquals((int)0,estudiante3.recibirPromedio());
    }
    @Test
    public void cantidadDeEstudiantesInscriptos() throws AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        Estudiante estudiante3 = new Estudiante("Josefina","Sabado", 18, 42949383, "Valle Viejo");

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertEquals((int)3,curso.cantidadDeEstudiantesInscriptos());
    }
    @Test
    public void estudiantes() throws ListaEstudiantesVaciaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        Estudiante estudiante3 = new Estudiante("Josefina","Sabado", 18, 42949383, "Valle Viejo");

        Curso curso = new Curso("1A", 20);  
        curso.agregarEstudiante(estudiante1);   // Ingreso de apellidos: Contreras -> Sabado -> Domingo
        curso.agregarEstudiante(estudiante3);
        curso.agregarEstudiante(estudiante2);

        assertEquals("Gustavo",curso.estudiantes().get(0).getNombre()); //    estudiantes() devuelve: 
        assertEquals("Juan",curso.estudiantes().get(1).getNombre());    //    Contreras - Domingo - Sabado
        assertEquals("Josefina",curso.estudiantes().get(2).getNombre());
    }
    @Test (expected = ListaEstudiantesVaciaException.class)
    public void estudiantesVacio() throws ListaEstudiantesVaciaException
    {
        Curso curso = new Curso("1A", 20);
        curso.estudiantes();
        
    }
    @Test
    public void estudiantesAprobados() throws PromedioIncalculableException, CalificacionNegativaException, ListaEstudiantesVaciaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(2);
        estudiante1.agregarCalificacion(3);   
        estudiante1.agregarCalificacion(4);

        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(7);
        estudiante2.agregarCalificacion(5);   
        estudiante2.agregarCalificacion(6);

        Estudiante estudiante3 = new Estudiante("Josefina","Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(9);
        estudiante3.agregarCalificacion(10);
        estudiante3.agregarCalificacion(8);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);   // Alumno desaprobado
        curso.agregarEstudiante(estudiante3);   // Alumno aprobado con mayor nota, Apellido: Sabado
        curso.agregarEstudiante(estudiante2);   // Alumno aprobado con menor nota, Apellido: Domingo

        assertEquals("Juan",curso.estudiantesAprobados().get(0).getNombre());       // Alumno de menor nota
        assertEquals("Josefina",curso.estudiantesAprobados().get(1).getNombre());   // Alumno de mayor nota

        boolean seEncuentra=false;
        for (Estudiante i : curso.estudiantesAprobados())
        {
            if (i.getNombre().equals("Gustavo") && i.getApellido().equals("Contreras"))
            {
                seEncuentra = true;
            }
        }
        assertFalse(seEncuentra);
    }
    @Test (expected = ListaEstudiantesVaciaException.class)
    public void EstudiantesAprobadosVacio() throws ListaEstudiantesVaciaException, PromedioIncalculableException
    {
        Curso curso = new Curso("1A", 20);
        curso.estudiantesAprobados();
    }
    @Test
    public void existeUnEstudianteTrue() throws AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);

        assertTrue(curso.existeEstudiante());
    }
    @Test
    public void existeMasDeUnEstudiante() throws AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        Estudiante estudiante3 = new Estudiante("Josefina","Sabado", 18, 42949383, "Valle Viejo");

        Curso curso = new Curso("1A", 20);      
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertTrue(curso.existeEstudiante());
    }
    @Test
    public void existeUnEstudianteFalse()
    {
        Curso curso = new Curso("1A", 20);
        assertFalse(curso.existeEstudiante());
    }
    @Test
    public void existeEstudianteConNotaDiezTrue() throws PromedioIncalculableException, CalificacionNegativaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(10);
        estudiante1.agregarCalificacion(10);   
        estudiante1.agregarCalificacion(10);
        
        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);

        assertTrue(curso.existeEstudianteConNotaDiez());
    }
    @Test
    public void existeEstudianteConNotaDiezFalse() throws PromedioIncalculableException, CalificacionNegativaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(5);
        estudiante1.agregarCalificacion(10);   
        estudiante1.agregarCalificacion(10);
        
        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);

        assertFalse(curso.existeEstudianteConNotaDiez());
    }
    @Test
    public void existeEstudianteLlamadoTrue() throws AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante2);

        assertTrue(curso.existeEstudianteLlamado("Juan","Domingo"));
    }
    @Test
    public void existeEstudianteLlamadoFalse() throws AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);

        assertFalse(curso.existeEstudianteLlamado("Juan","Domingo"));
    }
    @Test
    public void porcentajeDeAprobados() throws PromedioIncalculableException, CalificacionNegativaException, ListaEstudiantesVaciaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(2);
        estudiante1.agregarCalificacion(3);   
        estudiante1.agregarCalificacion(4);

        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(7);
        estudiante2.agregarCalificacion(5);   
        estudiante2.agregarCalificacion(6);

        Estudiante estudiante3 = new Estudiante("Josefina","Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(9);
        estudiante3.agregarCalificacion(10);
        estudiante3.agregarCalificacion(8);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertEquals((int)66,curso.porcentajeDeAprobados());
    }
    @Test
    public void promedioDeCalificaciones() throws PromedioIncalculableException, CalificacionNegativaException, ListaEstudiantesVaciaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(2);
        estudiante1.agregarCalificacion(3);   // Promedio = 3
        estudiante1.agregarCalificacion(4);

        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(7);
        estudiante2.agregarCalificacion(5);   // Promedio = 6
        estudiante2.agregarCalificacion(6);
        Estudiante estudiante3 = new Estudiante("Josefina","Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(9);
        estudiante3.agregarCalificacion(10); // Promedio = 9
        estudiante3.agregarCalificacion(8);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);   // Promedio general = 6
        curso.agregarEstudiante(estudiante3);

        assertEquals((int)6,curso.promedioDeCalificaciones());
    }
    @Test
    public void estudiantesDelInteriorProvincial() throws ListaEstudiantesVaciaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        Estudiante estudiante3 = new Estudiante("Josefina","Sabado", 18, 42949383, "Valle Viejo");

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante3);   
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);

        assertEquals("Juan",curso.estudiantesDelInteriorProvincial().get(0).getNombre());
        assertEquals("Josefina",curso.estudiantesDelInteriorProvincial().get(1).getNombre());
    }
    @Test
    public void ciudadesExceptoCapital() throws ListaEstudiantesVaciaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        Estudiante estudiante3 = new Estudiante("Josefina","Sabado", 18, 42949383, "Valle Viejo");

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante3);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante1);

        assertEquals("Fray Mamerto Esquiu",curso.ciudadesExceptoCapital().get(0));
        assertEquals("Valle Viejo",curso.ciudadesExceptoCapital().get(1));
    }
    @Test
    public void unDesastreTrue() throws PromedioIncalculableException, CalificacionNegativaException, ListaEstudiantesVaciaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(4);
        estudiante1.agregarCalificacion(3);   
        estudiante1.agregarCalificacion(4);

        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(1);
        estudiante2.agregarCalificacion(2);   
        estudiante2.agregarCalificacion(3);

        Estudiante estudiante3 = new Estudiante("Josefina","Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(4);
        estudiante3.agregarCalificacion(5); 
        estudiante3.agregarCalificacion(1);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertTrue(curso.unDesastre());
    }
    @Test
    public void unDesastreFalse() throws PromedioIncalculableException, CalificacionNegativaException, ListaEstudiantesVaciaException, AtributoEstudianteIncorrectoException
    {
        Estudiante estudiante1 = new Estudiante("Gustavo","Contreras", 19, 43995185, "Capital");
        estudiante1.agregarCalificacion(7);
        estudiante1.agregarCalificacion(8);   
        estudiante1.agregarCalificacion(6);

        Estudiante estudiante2 = new Estudiante("Juan","Domingo",18,40399183,"Fray Mamerto Esquiu");
        estudiante2.agregarCalificacion(1);
        estudiante2.agregarCalificacion(2);   
        estudiante2.agregarCalificacion(3);

        Estudiante estudiante3 = new Estudiante("Josefina","Sabado", 18, 42949383, "Valle Viejo");
        estudiante3.agregarCalificacion(4);
        estudiante3.agregarCalificacion(5); 
        estudiante3.agregarCalificacion(1);

        Curso curso = new Curso("1A", 20);
        curso.agregarEstudiante(estudiante1);
        curso.agregarEstudiante(estudiante2);
        curso.agregarEstudiante(estudiante3);

        assertFalse(curso.unDesastre());
    }
}
