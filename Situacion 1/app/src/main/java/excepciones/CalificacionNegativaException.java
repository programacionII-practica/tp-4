package excepciones;

public class CalificacionNegativaException extends Exception 
{
    public CalificacionNegativaException(String mensaje)
    {
        super(mensaje);
    }    
}
