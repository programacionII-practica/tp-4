package excepciones;

public class ListaEstudiantesVaciaException extends Exception{
    public ListaEstudiantesVaciaException(String mensaje)
    {
        super(mensaje);
    }
}
