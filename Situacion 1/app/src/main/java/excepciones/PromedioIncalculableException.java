package excepciones;

public class PromedioIncalculableException extends Exception
{
    public PromedioIncalculableException(String mensaje)
    {
        super(mensaje);
    }
}