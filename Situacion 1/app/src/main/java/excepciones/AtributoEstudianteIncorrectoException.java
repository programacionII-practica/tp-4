package excepciones;

public class AtributoEstudianteIncorrectoException extends Exception
{
    public AtributoEstudianteIncorrectoException(String mensaje)
    {
        super(mensaje);
    }
}
