package situacion;
import java.util.ArrayList;

import excepciones.AtributoEstudianteIncorrectoException;
import excepciones.CalificacionNegativaException;
import excepciones.PromedioIncalculableException;
import excepciones.NumeroInvalidoException;

public class Estudiante {
    private String nombre;
    private String apellido;
    private int edad;
    private int dni;
    private String lugarNacimiento;
    private ArrayList<Integer> calificacion;
    

    public Estudiante(String nombre,String apellido, int edad,int dni,String lugarNacimiento) throws AtributoEstudianteIncorrectoException
    {
        if (edad > 0 && dni > 0)
        {
            this.nombre = nombre;
            this.apellido = apellido;
            this.edad = edad;
            this.dni = dni;
            this.lugarNacimiento = lugarNacimiento;

            calificacion = new ArrayList<Integer>();
        }
        else
        {
            throw new AtributoEstudianteIncorrectoException("Se ha ingresado edad y/o dni no valido del alumno.");            
        }
        
    }

    public String getApellido() {
        return apellido;
    }
    public void agregarCalificacion(int calificacion) throws CalificacionNegativaException
    {
        if (calificacion >= 0 && calificacion < 11)
        {
            this.calificacion.add(calificacion);
        }
        else
        {
            throw new CalificacionNegativaException("Se intento agregar una calificacion negativa");
        }
    }
    public int recibirPromedio() throws PromedioIncalculableException
    {
        int promedio=0;
            if (!this.calificacion.isEmpty())
            {
                promedio = 0;
                for (int i : this.calificacion)
                {
                   promedio += i;
                }
        }
        if (this.calificacion.size() > 0)
        {
            promedio = promedio/this.calificacion.size();
        }
        else
        {
            throw new PromedioIncalculableException("Es imposible calcular promedio. La cantidad de califaciones debe ser 1 o mayor.");
        }
        return promedio;
    }
    public String getLugarNacimiento()
    {
        return this.lugarNacimiento;
    }
    public String getNombre()
    {
        return this.nombre;
    }
    public int getEdad()
    {
        return this.edad;
    }
    public int getDNI()
    {
        return this.dni;
    }
    public ArrayList<Integer> getCalificacion()
    {
        return this.calificacion;
    }
}
