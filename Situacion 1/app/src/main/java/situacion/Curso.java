package situacion;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import excepciones.ListaEstudiantesVaciaException;
import excepciones.PromedioIncalculableException;

import java.util.Comparator;
import java.util.Collections;

public class Curso 
{
    private String nombreCurso;
    private int codigo;
    private ArrayList<Estudiante> estudiantes;
    
    public Curso(String nombreCurso,int codigo)
    {
        this.nombreCurso = nombreCurso;
        this.codigo = codigo;
        estudiantes = new ArrayList<>();
    }
    public String getNombreCurso()
    {
        return this.nombreCurso;
    }
    public int getCodigo()
    {
        return this.codigo;
    }

    public boolean resetearNotas()
    {
        for (Estudiante estudiante : estudiantes)
        {
            estudiante.getCalificacion().clear();
            estudiante.getCalificacion().add(0);
        }

        return true;
    }

    public void agregarEstudiante(Estudiante estudiante)
    {
        this.estudiantes.add(estudiante);
    }
    public int cantidadDeEstudiantesInscriptos()
    {
        return this.estudiantes.size();
    }
    public ArrayList<Estudiante> estudiantes() throws ListaEstudiantesVaciaException
    {
        if (!this.estudiantes.isEmpty())
        {
            Collections.sort(this.estudiantes,new Comparator<Estudiante>()
        {
            @Override
            public int compare(Estudiante o1, Estudiante o2) 
            {
                // compare two instance of `Score` and return `int` as result.
                return o1.getApellido().compareTo(o2.getApellido());
            }
        });
        }
        else
        {
            throw new ListaEstudiantesVaciaException("No hay alumnos registrados en la lista");
        }
        return this.estudiantes;
    }
    public ArrayList<Estudiante> estudiantesAprobados() throws PromedioIncalculableException, ListaEstudiantesVaciaException
    {
        if (!this.estudiantes.isEmpty())
        {
            ArrayList<Estudiante> estudiantesAprobados = new ArrayList<Estudiante>();
            for (Estudiante i : this.estudiantes)
            {
                if (i.recibirPromedio() >= 4)
                {
                    estudiantesAprobados.add(i);
                }   
            }
            Collections.sort(estudiantesAprobados,new Comparator<Estudiante>()
            {
                @Override
                public int compare(Estudiante o1, Estudiante o2){
                    int comparacion;
                    try
                    {
                        comparacion = Integer.compare(o1.recibirPromedio(), o2.recibirPromedio());
                        return comparacion;
                    }
                    catch(PromedioIncalculableException e)
                    {
                        System.out.println("Error al intentar comparar promedio. Promedio incalculable");
                        return 0;
                    }
                }
            });
            return estudiantesAprobados;
        }
        else
        {
            throw new ListaEstudiantesVaciaException("mensaje");
        }
        
    }
    public boolean existeEstudiante()
    {
        return !estudiantes.isEmpty();
    }
    public boolean existeEstudianteConNotaDiez() throws PromedioIncalculableException
    {
        boolean existe=false;
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.recibirPromedio() == 10)
            {
                existe = true;
            }
        }
        return existe;
    }
    public boolean existeEstudianteLlamado(String nombre, String apellido)
    {
        boolean existe=false;
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.getNombre().equals(nombre) && estudiante.getApellido().equals(apellido))
            {
                existe = true;
            }
        }
        return existe;
    }
    public Estudiante buscarEstudiante(String nombre, String apellido)
    {
        if (existeEstudianteLlamado(nombre,apellido))
        {
            for (Estudiante estudiante : this.estudiantes)
            {
              if (estudiante.getNombre().equals(nombre) && estudiante.getApellido().equals(apellido))
              {
                  return estudiante;
              }
            }
        }
        return null;
    }
    public boolean eliminarEstudiante(String nombre, String apellido)
    {
        if (this.existeEstudianteLlamado(nombre,apellido))
        {
            this.estudiantes.remove(this.buscarEstudiante(nombre, apellido));
            return true;
        }
        else
        {
            return false;
        }
    }
    public int porcentajeDeAprobados() throws PromedioIncalculableException,ListaEstudiantesVaciaException
    {
        int cantidadAprobados=0;
        if (!this.estudiantes.isEmpty())
        {
            for (Estudiante estudiante : this.estudiantes)
            {
                if (estudiante.recibirPromedio() >= 4)
                {
                    cantidadAprobados += 1;
                }
            }
            return (cantidadAprobados*100)/estudiantes.size();
        }
        else
        {
            throw new ListaEstudiantesVaciaException("");
        }

    }
    public int promedioDeCalificaciones() throws PromedioIncalculableException, ListaEstudiantesVaciaException
    {
        int cantidadPromedios=0;
        if (!this.estudiantes.isEmpty())
        {
            for (Estudiante estudiante : this.estudiantes)
            {
                cantidadPromedios += estudiante.recibirPromedio();
            }
            return cantidadPromedios/estudiantes.size();
        }
        else
        {
            throw new ListaEstudiantesVaciaException("mensaje");
        }
    }
    public ArrayList<Estudiante> estudiantesDelInteriorProvincial() throws ListaEstudiantesVaciaException
    {
        if (!this.estudiantes.isEmpty())
        {
            ArrayList<Estudiante> estudiantesDelInterior = new ArrayList<Estudiante>();
            for (Estudiante estudiante : this.estudiantes)
            {
                if (!"Capital".equals(estudiante.getLugarNacimiento()))
                {
                    estudiantesDelInterior.add(estudiante);
                }
            }
            Collections.sort(estudiantesDelInterior,new Comparator<Estudiante>()
            {
                @Override
                public int compare(Estudiante o1,Estudiante o2)
                {
                    int c;
                    c = o1.getApellido().compareTo(o2.getApellido());
                    if (c == 0)
                    {
                        c = o1.getLugarNacimiento().compareTo(o2.getLugarNacimiento());
                    }
                    return c;
                }
            });
            return estudiantesDelInterior;
        }
        else
        {
            throw new ListaEstudiantesVaciaException("mensaje");
        }
        
    }
    public ArrayList<String> ciudadesExceptoCapital() throws ListaEstudiantesVaciaException
    {
        if (!this.estudiantes.isEmpty())
        {
            ArrayList<String> ciudades = new ArrayList<String>();
            for (Estudiante estudiante : this.estudiantes)
            {
                if (!"Capital".equals(estudiante.getLugarNacimiento()))
                {
                    ciudades.add(estudiante.getLugarNacimiento());
                }
            }

            Set<String> ciudadesNoRepetidas = new HashSet<String>(ciudades);
            ciudades.clear();
            ciudades.addAll(ciudadesNoRepetidas);

            Collections.sort(ciudades,new Comparator<String>()
            {
                @Override
                public int compare(String o1, String o2)
                {
                    return o1.compareTo(o2);
                }
            });
            return ciudades;
        }
        else
        {
            throw new ListaEstudiantesVaciaException("mensaje");
        }
    }
    public boolean unDesastre() throws PromedioIncalculableException, ListaEstudiantesVaciaException
    {
        if (!this.estudiantes.isEmpty())
        {
            boolean desastre=true;
            for (Estudiante estudiante : this.estudiantes)
            {
                if (estudiante.recibirPromedio() >= 4)
                {
                    desastre = false;
                }
            }
            return desastre;
        }
        else
        {
            throw new ListaEstudiantesVaciaException("mensaje");
        }
    }


}
