/*
• #resetearNotas
    "Pone en cero las calificaciones de todos los estudiantes. "
• #agregarEstudiante: unEstudiante
    "Agrega unEstudiante al curso"
• #cantidadDeEstudiantesInscriptos
    "Retorna la cantidad de alumnos que se inscribieron al curso"
• #estudiantes
    "Retorna la colección de estudiantes del curso"
• #estudiantesAprobados
    "Retorna una colección con todos los estudiantes que aprobaron el curso (calificación
    superior o igual a 4)"
• #existeEstudiante: unEstudiante
    "Indica si unEstudiante se encuentra inscripto en el curso"
• #existeEstudianteConNotaDiez
    "Determina si algún alumno obtuvo la calificación 10"
• #existeEstudianteLlamado: aString
    "Indica si el estudiante llamado aString se encuentra inscripto en el curso"
• #porcentajeDeAprobados
    "Retorna en porcentaje de estudiantes aprobados"
• #promedioDeCalificaciones
    "Calcula el promedio de las calificaciones obtenidas por los alumnos"
• #estudiantesDelInteriorProvinial
    "Retorna una colección con todos los estudiantes que no nacieron en la capital"
• #ciudadesExceptoCapital
    "Retorna una colección, sin repeticiones, conteniendo los nombres de todas las
    ciudades donde nacieron los alumnos inscriptos al curso"
• #unDesastre 
    "Retorna verdadero si todos los estudiantes desaprobaron el curso" 
*/
package situacion;

import excepciones.AtributoEstudianteIncorrectoException;
import excepciones.CalificacionNegativaException;
import excepciones.ListaEstudiantesVaciaException;
import excepciones.PromedioIncalculableException;
import interfaz.Ventana;

public class App {
    public static void main(String args[]) throws CalificacionNegativaException, PromedioIncalculableException,ListaEstudiantesVaciaException, AtributoEstudianteIncorrectoException
    {
        Ventana ventana = new Ventana();
        ventana.setVisible(true);
    }
}
